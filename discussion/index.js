// JS ES6 Updates

//1. Exponent Operator
	//pre-es6
	const firstNum = Math.pow(8, 2);
	console.log (firstNum)

	//es6
	const secondNum = 8**2;
	console.log(secondNum)

//2. Template Literals
	//allows writing of strings without using the concat operator; greatly helps with code readability
		//pre-es6
			//uses single quotes
			let name = 'John'
			let message = ' Hello ' + name + '! Welcome to programming'
			console.log("Message without template literals:" + message)
		//es6
			//uses backticks
			message = `Hello ${name}! Welcome to programming`
			console.log(`Message with template literals: ${message}`)

			// Multiline
			const anotherMessage = `
			${name} attended a math competition.
			He won it by solving the problem 8**2 with solution of ${secondNum}.`
			console.log(anotherMessage)
		
			// computation inside o. literals
			const interestRate = .1;
			const principal = 1000;
			console.log(`The interest on your savings account is: ${principal * interestRate}`)


// 3. Array Destructuring
	// allows unpacking elements in arrays into distinct variables; allows naming of array elements with variables instead of using index numbers; helps with code readability.
	//let/const [variable, variable, nthVariable] = array

	const fullName = ['Juan', 'Dela', 'Cruz'];
		// pre-es6
		console.log(fullName[0]);
		console.log(fullName[1]);
		console.log(fullName[2]);
		console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);
		

		//es6
		const [firstName, middleName, lastName] = fullName
		console.log(firstName);
		console.log(middleName);
		console.log(lastName);
		console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);



//4. Object Destructuring
	// allows unpacking elements in objects into distinct variables; shortens the syntax for accessing objects
		//let/const {propertyName, propertyName, nthPropertyName} = object
	const person = {
		givenName:'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz'
	}
	// pre-es6
	console.log(person.givenName)
	console.log(person.maidenName)
	console.log(person.familyName)

	console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again! `)

	// es6
	const {givenName, maidenName, familyName} = person;
	console.log(givenName)
	console.log(person.maidenName)
	console.log(person.familyName)
	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again! `)
	//using function 
	function getFullName({ givenName, maidenName, familyName }) {
		console.log(`${givenName} ${maidenName} ${familyName}`)
	}
	getFullName(person)

// 5. Arrow Function
			//compact alternative syntax to traditional functions; useful for code snippets where creating functions will not be reused in any other portions of the code; adheres to dry principle('Dont Repeat Yourself') where there's no need to create a function and think a name for a function that will be used in certain snippets

		//pre-es6
			// PARTS*************
				//function declaration
				//function name
				//parameters
				//statements
				//invoke/callback function

			function printFullName(fname, mname, lname) {
				console.log(fname+' '+ mname+' '+lname)
			}
			console.log('John','D.', 'Smith')

		//es6

		const printFName = (fname, mname, lname) =>{
			console.log(`${fname} ${mname} ${lname}`)
		};
		console.log('John','D.', 'Smith')

		//arrow function with loops

		const students = ['John','Jane','Joe'];
		students.forEach (function (student){
			console.log(`${student} is a student`)
		})
		// ****
		students.forEach((student) => console.log(`${student} is a student.`));


//6. (ArrowFunction) Implicit return statement
	// return statements are omitted. this is because even without them, JS implicitly adds them for the result of the function
	// Pre-es6
		const add = (x,y) =>{
			return x+y;
		}
		let total = add(1,2)
		console.log(total)
	// es6
		const adds = (x,y)=> x+y
		let totals = adds(1,2)
		console.log(totals)
		// USING FUNCTION
		let friends = [`none`]
		let filterFriends = friends.filter(
			friend => friend.length === 4)
		// friends.filter((friend, foe)) - add parenthesis in the parameter if there are 2 or more.

// 7. (ArrowFunction) Default Function Argument Value
	// Provides a default argument value if none is provided when the function is invoked 

	const greet = (name = 'User')=> {
		return `Good morning, ${name}`
	}
console.log(greet())

//8. Creating a Class
	/*
		class className{
			constructor (objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA
				this.objectPropertyB = objectPropertyB
			}
		}

		***constructo - special method of class for creating/initializing an object for that class***
		***this - properies of an object created from the class.***
	*/
	class car{
		constructor (brand, name, year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}
	const car1 = new car();
	console.log(car1);
	car1.brand = 'Ford';
	car1.name = 'Ranger Raptor';
	car1.year = '2021';
	console.log(car1);
	// Creating/instantiating a new object from the car class with initialized value
	const car2 = new car ('Toyota', 'Vios', 2021)
	console.log(car2);

//9. Ternary Operator
	// conditional operator - has 3 operands
	// *condition, questionmark, expression to execute if the condition is true followed by colon and finally the expression to execute if the condition is false.
		/*
		if (condition = 0){
			return true
		} else {
			return false
		}
		*/
		/*
		(condition = 0) ? true : false
		*/
	// 
	// 
	// 
	// 
	// 
	// 
	// 
	// 
	// 
	// 
	// 
	// 


