// ***EXPONENT**
let num = 24
const getCube = num**3
console.log(`The cube of ${num} is ${getCube}.`);


//**ADDRESS**
const completeAddress = ['258', 'Washington Ave NW', 'California', '90011'];

const [HouseBlockNumber, Street, State, Code] = completeAddress
console.log(`I live at ${HouseBlockNumber}, ${Street}, ${State}, ${Code}.`)


const lolong = {
		name:'Lolong',
		type: 'saltwater crocodile',
		weight: '1075 kgs',
		length: '20 ft 3 in'
	}
	const {name, type, weight, length} = lolong
	console.log(`${name} is a ${type}. He weighed at ${weight} with a measurement of ${length}`)


//**NUMBERS**
let numbers = [1,2,3,4,5]
numbers.forEach((number) => console.log(`${number}`));

let reduceNumber = numbers.reduce(function(a, b){
	return a+b
})
console.log(reduceNumber);

//**DOG**
class Dog {
	constructor(name, age, breed){
		this.name=name;
		this.age=age;
		this.breed=breed
	}
}

const Dog1 = new Dog('Frankie', 5, 'Miniature Dachshund')
console.log(Dog1)

const Dog2 = new Dog('Burnok', 6, 'Stripes')
console.log(Dog2)